import React from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';
import './App.scss';
import { Routes } from './routes/routes';
import { Header } from './components';

const App = () => {
  return (
    <div className='App'>
      <BrowserRouter>
        <Header />

        <Switch>
          {Routes.map((route: any) => (
            <Route
              key={route.key}
              path={route.path}
              exact={route.exact}
              render={(renderProps: object) => (
                <route.component {...renderProps} />
              )}
            />
          ))}
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;