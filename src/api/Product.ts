import { Api } from '../helpers';
import { Product } from '../models';

interface ProductInterface {
  fetchAllProducts(): Promise<Product[]>;
  postNewProduct(product: Product): Promise<Product>;
  getProductsBy(query: string): Promise<Product[]>;
}

class ProductApi implements ProductInterface {
  fetchAllProducts(): Promise<Product[]> {
    return Api.http({
      method: 'get',
      url: '/products',
    });
  }

  getProductsBy(query: string): Promise<Product[]> {
    return Api.http({
      method: 'get',
      url: `/products?${query}`,
    });
  }

  postNewProduct(product: Product): Promise<Product> {
    return Api.http({
      method: 'post',
      url: '/products',
      body: product,
    });
  }
}

export default new ProductApi();
