import ProductApi from "./Product";
import StateApi from './State';
import CategoryApi from "./Category";

export {
  ProductApi,
  StateApi,
  CategoryApi
}
