import { Api } from '../helpers';
import { Category } from '../models';

interface CategoryInterface {
  fetchAllCategories(): Promise<Category[]>;
}

class CategoryApi implements CategoryInterface {
  fetchAllCategories(): Promise<Category[]> {
    return Api.http({
      method: 'get',
      url: '/categories',
    });
  }
}

export default new CategoryApi();
