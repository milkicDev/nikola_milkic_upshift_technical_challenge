import { Api } from '../helpers';
import { State } from '../models';

interface StateInterface {
  fetchAllStates(): Promise<State[]>;
}

class StateApi implements StateInterface {
  fetchAllStates(): Promise<State[]> {
    return Api.http({
      method: 'get',
      url: '/states',
    });
  };
}

export default new StateApi();
