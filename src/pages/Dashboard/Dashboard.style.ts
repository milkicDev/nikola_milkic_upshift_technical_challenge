import styled, { css } from 'styled-components';

const flex = css`
  display: flex;
  gap: 20px;
  width: 100%;
`;

export const Wrapper = styled.div`
  ${flex};
  margin-top: 20px;
`;

export const Header = styled.div`
  ${flex};
  margin-top: 20px;
  justify-content: space-between;
  align-items: center;
`;

export const AddButton = styled.div`
  ${flex};
  gap: 5px;
  width: fit-content;
  font-size: 20px;
  color: #2081DB;
  cursor: pointer;

  & .icon {
    ${flex}
    align-items: center;
    justify-content: center;

    width: 24px;
    height: 24px;
    border: 2px solid #2081DB;
    border-radius: 50px;

    padding: 4px;

    & svg {
      width: 100%;
      height: 100%;

      & path {
        fill: #2081DB;
      }
    }
  }
`;

export const SelectStyled = styled.select`
  width: 100%;
  height: 40px;
  color: #fff;
  background-color: #2A2C2D;
  border: none;
  padding: 10px;
  outline: none;
`;