import React from 'react';
import { connect } from 'react-redux';
import * as Styled from './Dashboard.style';
import {
  AddProductForm,
  ContentBox,
  Modal,
  ProductListItem,
} from '../../components';
import { ReactComponent as Plus } from '../../assets/plus.svg';
import { Category, Product, State } from '../../models';
import { ProductActions } from '../../store/Actions/Product';
import { InitialState } from '../../store';
import { StateActions } from '../../store/Actions/State';
import { CategoryActions } from '../../store/Actions/Category';
import { ProductApi } from '../../api';

export interface DashboardProps {
  getProducts: Function;
  products: Product[];
  getStates: Function;
  getCategories: Function;
  states: State[];
  categories: Category[];
}

interface DashboardState {
  selectedProduct: Product | null | undefined;
  showAddModal: boolean;
  sortBy: string;
  products: Product[];
}

class Dashboard extends React.Component<DashboardProps, DashboardState> {
  loading: boolean = true;
  state: DashboardState = {
    selectedProduct: null,
    showAddModal: false,
    sortBy: '',
    products: [],
  };

  componentDidMount() {
    this.props.getProducts();
    this.props.getCategories();
    this.props.getStates();
  }

  componentDidUpdate() {
    if (this.loading && this.state.products.length !== this.props.products.length) {
      this.loading = false;
      this.setState({ products: this.props.products });
    }
  }

  setAddModalValue(value: boolean) {
    if (this.state.showAddModal === value) {
      return;
    }

    this.setState({ showAddModal: value });
  }

  setSelectedProduct(Product: Product | null) {
    if (this.state.selectedProduct === Product) {
      return;
    }

    this.setState({ selectedProduct: Product });
  }

  sortBy = (value: string, Products?: Product[]) => {
    const values = value.split('_');
    const products = Products || [...this.state.products];

    products.sort((productA, productB) => {
      const a = productA[values[0] as keyof Product];
      const b = productB[values[0] as keyof Product];

      if (values[1] === 'desc') {
        if (a && b && a < b) {
          return 1;
        } else if (a && b && a > b) {
          return -1;
        }
      } else {
        if (a && b && a > b) {
          return 1;
        } else if (a && b && a < b) {
          return -1;
        }
      }

      return 0;
    });

    this.setState({ sortBy: value, products });
  };

  getBy(type: string, value: string) {
    const query = `${type}=${value}`;

    ProductApi.getProductsBy(query)
      .then((res) => this.sortBy(this.state.sortBy, res))
      .catch((err) => console.error(err));
  }

  render() {
    return (
      <div className='container'>
        <Styled.Header>
          <Styled.Wrapper style={{ width: '40%' }}>
            <Styled.SelectStyled
              onChange={(e) => this.sortBy(e.target.value)}
              value={this.state.sortBy ?? ''}
            >
              <option value=''>None</option>

              <option value='price_asc'>Price Lowest</option>
              <option value='price_desc'>Price Highest</option>

              <option value='title_asc'>Title Ascending</option>
              <option value='title_desc'>Title Descending</option>
            </Styled.SelectStyled>
            <Styled.SelectStyled
              onChange={(e) => this.getBy('categoryId', e.target.value)}
              value={this.state.sortBy ?? ''}
            >
              <option value=''>None</option>
              {this.props.categories.map((category) => (
                <option key={category.id} value={category.id?.toString()}>
                  {category.name}
                </option>
              ))}
            </Styled.SelectStyled>
            <Styled.SelectStyled
              onChange={(e) => this.getBy('stateId', e.target.value)}
              value={this.state.sortBy ?? ''}
            >
              <option value=''>None</option>

              {this.props.states.map((state) => (
                <option key={state.id} value={state.id?.toString()}>
                  {state.name}
                </option>
              ))}
            </Styled.SelectStyled>
          </Styled.Wrapper>

          <>
            <Styled.AddButton onClick={() => this.setAddModalValue(true)}>
              <div className='icon'>
                <Plus />
              </div>
              Add Product
            </Styled.AddButton>
          </>
        </Styled.Header>
        <Styled.Wrapper>
          <ContentBox title='Products' style={{ width: '30%' }}>
            {this.state.products.map((product) => (
              <ContentBox
                key={product.id}
                style={{
                  background: 'rgba(86, 89, 90, 0.15)',
                  cursor: 'pointer',
                  marginBottom: '10px',
                }}
                onClick={() => this.setSelectedProduct(product)}
              >
                <ProductListItem {...product} />
              </ContentBox>
            ))}
          </ContentBox>

          <ContentBox>
            {this.state.selectedProduct && <Styled.Wrapper style={{ flexWrap: 'wrap', margin: 0 }}>
              <ContentBox
                style={{
                  width: 'calc(50% - 10px)',
                  background: 'rgba(86, 89, 90, 0.15)',
                }}
              >
                {this.state.selectedProduct?.id} -{' '}
                {this.state.selectedProduct?.title}
              </ContentBox>

              <ContentBox
                style={{
                  width: 'calc(50% - 10px)',
                  background: 'rgba(86, 89, 90, 0.15)',
                  textAlign: 'right',
                }}
              >
                {this.state.selectedProduct?.stock ? 'On Stock' : 'Out of Stock'}
                <br />${this.state.selectedProduct?.price}
              </ContentBox>

              <img
                src={this.state.selectedProduct?.picture}
                alt={this.state.selectedProduct?.title}
                width='100%'
              />

              <ContentBox
                style={{
                  background: 'rgba(86, 89, 90, 0.15)',
                }}
              >
                {this.state.selectedProduct?.description}
              </ContentBox>
            </Styled.Wrapper>}
          </ContentBox>

          <Modal
            show={this.state.showAddModal}
            close={() => this.setAddModalValue(false)}
            title='Create Product'
          >
            <AddProductForm afterSubmit={() => this.setAddModalValue(false)} />
          </Modal>
        </Styled.Wrapper>
      </div>
    );
  }
}

const mapStateToProps = (state: InitialState) => ({
  products: state.ProductState.products,
  states: state.StateState.states,
  categories: state.CategoryState.categories,
});

const mapDispatchToProps = (dispatch: any) => ({
  getProducts: async () => {
    await dispatch(ProductActions.fetchAllProducts());
  },
  getStates: async () => {
    await dispatch(StateActions.fetchAllStates());
  },
  getCategories: async () => {
    await dispatch(CategoryActions.fetchAllCategories());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
