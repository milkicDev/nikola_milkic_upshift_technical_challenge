import React from 'react';
import * as Styled from './Header.style';
import logo from '../../assets/logo.svg';

export interface HeaderProps {}

export const Header: React.FC<HeaderProps> = (props) => {
  return (
    <Styled.Holder>
      <Styled.Wrapper>
        <div className='Brand'>
          <img src={logo} alt={'App Logo'} />
        </div>
        <span>...</span>
      </Styled.Wrapper>
    </Styled.Holder>
  );
};
