import styled from 'styled-components';

export const Holder = styled.header`
  width: 100%;
  height: 55px;

  background-color: #2081DB;
  color: white;
`;

export const Wrapper = styled.div`
  width: calc(100% - 30px);
  height: 100%;
  margin: 0 auto;
  display: flex;
  justify-content: space-between;
  align-items: center;

  padding: 5px 0;

  & .Brand {
    width: auto;
    height: 100%;

    & img {
      width: 100%;
      height: 100%;
    }
  }
`;
