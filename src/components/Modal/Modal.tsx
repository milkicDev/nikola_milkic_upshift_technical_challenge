import React from 'react';
import * as Styled from './Modal.style';
import { ReactComponent as Close } from '../../assets/close.svg';

export interface ModalProps {
  show: boolean;
  title?: string;
  close?: Function;
}

export const Modal: React.FC<ModalProps> = (props) => {
  const onClickOutside: React.MouseEventHandler<HTMLDivElement> = (e) => {
    if (e.target !== e.currentTarget) {
      return;
    }

    console.info('[ModalPopup]: Click outside modal');

    closeOnClick(e);
  };

  const closeOnClick: React.MouseEventHandler<HTMLDivElement> = (e) => {
    if (!props.close) {
      return;
    }
    
    props.close();
  }

  if (!props.show) {
    return null;
  }

  return (
    <Styled.Overlay onClick={onClickOutside}>
      <Styled.Popup>
        {props.close && (
          <Styled.Close onClick={closeOnClick}><Close /></Styled.Close>
        )}

        <Styled.H1>{props.title}</Styled.H1>

        {props.children}
      </Styled.Popup>
    </Styled.Overlay>
  );
};