import styled from 'styled-components';

export const Overlay = styled.div`
  position: fixed;
  top: 0;
  left: 0;

  width: 100%;
  height: 100%;

  background-color: rgba(0, 0, 0, 0.5);

  display: flex;
  align-items: center;
  justify-content: center;
`;

export const Popup = styled.div`
  position: relative;

  width: 60%;
  height: fit-content;

  background-color: #2A2C2D;
  color: #fff;
  padding: 25px 30px;
  border: 1px solid #000;
`;

export const H1 = styled.h3`
  font-size: 26px;
  font-weight: 300;
  text-transform: uppercase;
`;

export const Close = styled.div`
  position: absolute;
  top: 25px;
  right: 30px;

  cursor: pointer;
`