import React from 'react';
import { ID } from '../../helpers';
import * as Styled from './ProductListItem.style';

export interface ProductListItemProps {
  id: ID;
  title: string;
  price: number;
  stock: boolean;
}

export const ProductListItem: React.FC<ProductListItemProps> = ({
  id,
  title,
  price,
  stock,
}) => {
  return (
    <>
      <Styled.FlexWrapper style={{ margin: '0 0 10px' }}>
        <span>{id}</span>
        <span>{title}</span>
        <span>{stock ? 'On Stock' : 'Out of Stock'}</span>
      </Styled.FlexWrapper>
      <div style={{ textAlign: 'right' }}>${price}</div>
    </>
  );
};