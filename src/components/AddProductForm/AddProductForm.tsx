import React from 'react';
import * as Yup from 'yup';
import { Formik, Field, Form } from 'formik';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { Category, Product, State } from '../../models';
import { InitialState } from '../../store';
import { CategoryActions } from '../../store/Actions/Category';
import { StateActions } from '../../store/Actions/State';
import { ProductApi } from '../../api';
import { ProductActions } from '../../store/Actions/Product';

const FieldStyled = styled(Field)`
  width: 100%;
  height: 40px;
  color: #fff;
  background-color: rgba(86, 89, 90, 0.15);
  border: none;
  padding: 10px;
  outline: none;
`;

const FlexWrap = styled.div`
  display: flex;
  gap: 20px;
`;

const LabelStyled = styled.label`
  display: inline-block;
  margin-top: 20px;
  margin-bottom: 5px;
`;

const FlexItem = styled.div`
  width: 50%;
`;

export interface AddProductFormProps {
  afterSubmit?: Function;
  fetchAllProducts: Function;
  getStates: Function;
  getCategories: Function;
  states: State[];
  categories: Category[];
}

const AddProductForm: React.FC<AddProductFormProps> = (props) => {
  React.useEffect(() => {
    if (!props.states.length) {
      props.getStates();
    }

    if (!props.categories.length) {
      props.getCategories();
    }
  }, [props]);

  const onSubmit = (values: Product) => {
    ProductApi.postNewProduct(values)
      .then(() => {
        if (props.afterSubmit) {
          props.afterSubmit();
        }

        props.fetchAllProducts();
      })
      .catch((error) => {
        console.error('Something went wrong: ', error);
      });
  };

  const ValidateSchema = () => {
    const message = 'Required';

    return Yup.object().shape({
      title: Yup.string().min(4, 'Minimum 4 characters').required(message),
      price: Yup.number().required(message),
      stateId: Yup.number().required(message),
      categoryId: Yup.number().required(message),
      description: Yup.string().required(message),
      picture: Yup.string().required(message),
    });
  };

  const ValidatePrice = (fieldValue: Number, values: Product) => {
    const state = props.states.find(({ id }) => id === Number(values.stateId));

    let minimum = 4;
    if (state?.tax && state?.tax > 0.25) {
      minimum = 7;
    }

    if (fieldValue < minimum) {
      return `Minimal value is ${minimum}`;
    }
  }

  const InitialValue: Product = {
    id: undefined,
    stock: false,
    title: '',
    stateId: undefined,
    categoryId: undefined,
    price: 0,
    picture: '',
    description: '',
  };

  return (
    <Formik
      initialValues={InitialValue}
      validationSchema={ValidateSchema()}
      onSubmit={onSubmit}
    >
      {({ values, errors, touched }) => (
        <Form>
          <FlexWrap>
            <FlexItem style={{ width: '20%' }}>
              <FieldStyled component='select' name='stateId'>
                <option value={''}>Select</option>
                {props.states.map((state) => (
                  <option key={state.id} value={Number(state.id)}>
                    {state.name}
                  </option>
                ))}
              </FieldStyled>
              {errors.stateId && touched.stateId && (
                <span>{errors.stateId}</span>
              )}
            </FlexItem>

            <FlexItem style={{ width: '20%' }}>
              <FieldStyled component='select' name='categoryId'>
                <option value={''}>Select</option>
                {props.categories.map((category) => (
                  <option key={category.id} value={Number(category.id)}>
                    {category.name}
                  </option>
                ))}
              </FieldStyled>
              {errors.categoryId && touched.categoryId && (
                <span>{errors.categoryId}</span>
              )}
            </FlexItem>
          </FlexWrap>

          <FlexWrap>
            <FlexItem>
              <LabelStyled htmlFor='title'>
                Title{' '}
                {errors.title && touched.title && (
                  <span> - {errors.title}</span>
                )}
              </LabelStyled>
              <FieldStyled id='title' name='title' placeholder='Title' />
            </FlexItem>

            <FlexItem>
              <LabelStyled htmlFor='price'>
                Price{' '}
                {errors.price && touched.price && (
                  <span> - {errors.price}</span>
                )}
              </LabelStyled>
              <FieldStyled
                id='price'
                name='price'
                type='number'
                placeholder='Price'
                validate={(fieldValue: Number) => ValidatePrice(fieldValue, values)}
              />
            </FlexItem>
          </FlexWrap>

          <LabelStyled htmlFor='picture'>
            Image Url
            {errors.picture && touched.picture && (
              <span> - {errors.picture}</span>
            )}
          </LabelStyled>
          <FieldStyled id='picture' name='picture' placeholder='Image Url' />

          <LabelStyled htmlFor='description'>
            Description
            {errors.description && touched.description && (
              <span> - {errors.description}</span>
            )}
          </LabelStyled>
          <FieldStyled
            style={{ height: 'auto' }}
            id='description'
            name='description'
            component='textarea'
            rows='5'
            placeholder='Description'
          />

          <FlexWrap style={{ justifyContent: 'end', marginTop: '20px' }}>
            <FlexItem style={{ width: 'auto' }}>
              <button type='reset'>Reset</button>
            </FlexItem>
            <FlexItem style={{ width: 'auto' }}>
              <button type='submit'>Submit</button>
            </FlexItem>
          </FlexWrap>
        </Form>
      )}
    </Formik>
  );
};

const mapStateToProps = (state: InitialState) => ({
  states: state.StateState.states,
  categories: state.CategoryState.categories,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchAllProducts: async () => {
    await dispatch(ProductActions.fetchAllProducts());
  },
  getStates: async () => {
    await dispatch(StateActions.fetchAllStates());
  },
  getCategories: async () => {
    await dispatch(CategoryActions.fetchAllCategories());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(AddProductForm);
