import styled from 'styled-components';

export const Holder = styled.div`
  width: 100%;

  background: #2A2C2D;
  color: #ffffff;
  border-radius: 5px;
  padding: 15px;
`;

export const H1 = styled.h3`
  font-size: 26px;
  font-weight: 300;
`;