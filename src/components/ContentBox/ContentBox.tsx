import React from 'react';
import * as Styled from './ContentBox.style';

export interface ContentBoxProps {
  title?: string;
  style?: React.CSSProperties;
  onClick?: React.MouseEventHandler<HTMLDivElement>;
}

export const ContentBox: React.FC<ContentBoxProps> = (props) => {
  return (
    <Styled.Holder onClick={props.onClick} style={props.style}>
      {props?.title && <Styled.H1>{props.title}</Styled.H1>}

      {props.children}
    </Styled.Holder>
  );
};