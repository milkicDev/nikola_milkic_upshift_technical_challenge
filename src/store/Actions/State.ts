import { StateApi } from '../../api';
import { State } from '../../models';

export enum ActionTypes {
  fetchAllStates = 'FETCH_ALL_STATES',
}

interface StateActionsInterface {
  fetchAllStates: Function;
}

export const StateActions: StateActionsInterface = {
  fetchAllStates: () => async (dispatch: Function) => {
    const States: State[] = await StateApi.fetchAllStates();

    dispatch({
      type: ActionTypes.fetchAllStates,
      response: States,
    });
  },
};
