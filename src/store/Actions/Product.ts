import { ProductApi } from '../../api';
import { Product } from '../../models';

export enum ActionTypes {
  fetchAllProducts = 'FETCH_ALL_PRODUCTS',
}

interface ProductActionsInterface {
  fetchAllProducts: Function;
}

export const ProductActions: ProductActionsInterface = {
  fetchAllProducts: () => async (dispatch: Function) => {
    const Products: Product[] = await ProductApi.fetchAllProducts();

    dispatch({
      type: ActionTypes.fetchAllProducts,
      response: Products,
    });
  },
};
