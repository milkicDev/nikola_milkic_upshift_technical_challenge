import { CategoryApi } from '../../api';
import { Category } from '../../models';

export enum ActionTypes {
  fetchAllCategories = 'FETCH_ALL_CATEGORIES',
}

interface CategoryActionsInterface {
  fetchAllCategories: Function;
}

export const CategoryActions: CategoryActionsInterface = {
  fetchAllCategories: () => async (dispatch: Function) => {
    const Categories: Category[] = await CategoryApi.fetchAllCategories();

    dispatch({
      type: ActionTypes.fetchAllCategories,
      response: Categories,
    });
  },
};
