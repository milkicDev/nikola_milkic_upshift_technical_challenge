import { AnyAction } from 'redux';
import { Product } from '../../models';
import { ActionTypes } from '../Actions/Product';

export interface InitialState {
  products: Product[];
}

export const ProductState = (
  state: InitialState = {
    products: [],
  },
  action: {
    type: ActionTypes,
    response: any
  }
) => {
  switch (action.type) {
    case ActionTypes.fetchAllProducts:
      return {
        ...state,
        products: action.response,
      };

    default:
      return state;
  }
};
