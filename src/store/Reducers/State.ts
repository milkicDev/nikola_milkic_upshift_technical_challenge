import { AnyAction } from 'redux';
import { State } from '../../models';
import { ActionTypes } from '../Actions/State';

export interface InitialState {
  states: State[];
}

export const StateState = (
  state: InitialState = {
    states: [],
  },
  action: {
    type: ActionTypes,
    response: any
  }
) => {
  switch (action.type) {
    case ActionTypes.fetchAllStates:
      return {
        ...state,
        states: action.response,
      };

    default:
      return state;
  }
};
