import { AnyAction } from 'redux';
import { Category } from '../../models';
import { ActionTypes } from '../Actions/Category';

export interface InitialState {
  categories: Category[];
}

export const CategoryState = (
  state: InitialState = {
    categories: [],
  },
  action: {
    type: ActionTypes,
    response: any
  }
) => {
  switch (action.type) {
    case ActionTypes.fetchAllCategories:
      return {
        ...state,
        categories: action.response,
      };

    default:
      return state;
  }
};
