import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

import {
  ProductState,
  InitialState as ProductInitialState,
} from './Reducers/Product';
import {
  StateState,
  InitialState as StateInitialState,
} from './Reducers/State';
import {
  CategoryState,
  InitialState as CategoryInitialState,
} from './Reducers/Category';

export interface InitialState {
  ProductState: ProductInitialState;
  StateState: StateInitialState;
  CategoryState: CategoryInitialState;
}

const composeEnhancers =
  (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const Reducers = combineReducers({
  ProductState,
  StateState,
  CategoryState,
});

export const Store = createStore(
  Reducers,
  composeEnhancers(applyMiddleware(thunk))
);
