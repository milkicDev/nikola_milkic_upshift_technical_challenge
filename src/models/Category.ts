import { ID } from "../helpers";

export interface Category {
  id: ID,
  name: string,
}