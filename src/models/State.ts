import { ID } from "../helpers";

export interface State {
  id: ID;
  name: string;
  tax: number;
}