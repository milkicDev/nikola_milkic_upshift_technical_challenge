import { ID } from "../helpers";

export interface Product {
  id: ID;
  title: string;
  price: number;
  stateId: ID;
  categoryId: ID;
  stock: boolean;
  picture: string;
  description: string;
}
