import { Dashboard } from "../pages";

export const Routes = [
  {
    path: '/',
    key: 'root',
    exact: true,
    component: Dashboard,
  }
];