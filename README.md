# Upshift Technical Challange
Developed a simple e-commerce app in the most popular framework ReactJS and used typescript.

## Libraries:
    - Axios
    - React-Router
    - React Redux & Thunk
    - Formik & Yup
    - Styled Components

### Getting Started
Clone repository and run `npm install` after that rename env.local.example to env.local and after renaming use `npm run start` to start a project.
